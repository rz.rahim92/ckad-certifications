# CKAD Certifications
## Certified Kubernetes Application Developer (CKAD) Exam Curriculum

This document provides the curriculum outline of the Knowledge, Skills and Abilities that
a Certified Kubernetes Application Developer (CKAD) can be expected to demonstrate.

**20% Application Design and Build** \
• Define, build and modify container images
• Understand Jobs and CronJobs
• Understand multi-container Pod design
patterns (e.g. sidecar, init and others)
• Utilize persistent and ephemeral volumes

**20% - Application Deployment** \
• Use Kubernetes primitives to implement
common deployment strategies (e.g. blue/
green or canary)
• Understand Deployments and how to
perform rolling updates
• Use the Helm package manager to deploy
existing packages

**15% - Application observability and maintenance** \
• Understand API deprecations
• Implement probes and health checks
• Use provided tools to monitor Kubernetes
applications
• Utilize container logs
• Debugging in Kubernetes

**25% - Application Environment, Configuration and Security** \
• Discover and use resources that extend
Kubernetes (CRD)
• Understand authentication, authorization
and admission control
• Understanding and defining resource
requirements, limits and quotas
• Understand ConfigMaps
• Create & consume Secrets
• Understand ServiceAccounts
• Understand SecurityContexts

**20% - Services & Networking** \
• Demonstrate basic understanding of
NetworkPolicies
• Provide and troubleshoot access to
applications via services
• Use Ingress rules to expose application


## Core Concepts
**What is Nodes if Kubernetes** \
In Kubernetes, a node is a worker machine that runs containerized applications. Nodes are the physical or virtual machines that form the underlying infrastructure of a Kubernetes cluster, and they are responsible for running the containers that make up the applications. 

Nodes are managed by the Kubernetes control plane, which schedules workloads to run on the nodes and monitors their health and availability. Each node has a unique hostname, IP address, and set of labels that can be used to identify it and schedule workloads to run on it. 

Nodes in Kubernetes typically run a container runtime, such as Docker, that allows them to run and manage containers. Nodes also run a set of Kubernetes services and daemons, such as kubelet, which is responsible for communicating with the control plane and managing the containers on the node. 

**What is cluster** \
In the context of Kubernetes, a cluster is a set of physical or virtual machines, called nodes, that are connected to each other and managed as a single unit. 

The Kubernetes cluster is the foundation of the Kubernetes system, and it is responsible for running and managing the containerized applications that make up a cloud-native environment. The cluster consists of multiple nodes, which are the worker machines that run containerized applications using a container runtime such as Docker. 

**What is Master** \
In Kubernetes, the master is the central control plane that manages the cluster's state and configuration. The Kubernetes master is responsible for managing the nodes in the cluster, scheduling workloads, monitoring the health and availability of nodes and containers, and handling any necessary repairs or scaling operations. 

The Kubernetes master consists of several components, including:

- API server: The API server is the front-end component of the Kubernetes control plane. It exposes the Kubernetes API, which is used by administrators, developers, and other components to interact with the cluster.
- etcd: etcd is a distributed key-value store that is used by the Kubernetes master to store the cluster's configuration data, including information about the nodes, pods, services, and other resources in the cluster.
- scheduler: The scheduler is responsible for determining where and when to run each workload in the cluster. It evaluates resource requirements, node availability, and other factors to determine the optimal placement for each workload.
- controller manager: The controller manager is a set of controllers that are responsible for maintaining the desired state of the cluster. This includes monitoring the state of nodes and containers, handling scaling operations, and ensuring that the cluster is running according to the desired configuration.

**Kubernetes Master node Vs Worker node**
- Master Node: The master node is the control plane of the Kubernetes cluster. It is responsible for managing the overall state of the cluster, including scheduling workloads, monitoring node and container health, and handling scaling and replication operations. The master node consists of several components, including the API server, etcd, scheduler, and controller manager.

- Worker Node: On the other hand, the worker node is a compute node that runs the containerized applications. It is responsible for running the containers using a container runtime, such as Docker, and ensuring that they are running correctly. The worker node also communicates with the master node to receive instructions about which containers to run and how to manage them.

 **Docker and Containerd what are the key Differences!!**
- Architecture: 
Docker uses a client-server architecture where the Docker daemon is responsible for managing the lifecycle of containers, while the Docker client communicates with the daemon to manage containers. \ 
Containerd, on the other hand, has a more modular architecture that allows it to integrate with other container management tools and orchestrators.

- Scope: 
Docker provides a more comprehensive solution for container management, with a broader set of features and tools. Docker includes a registry for storing and sharing container images, as well as tools for managing container networks and volumes. \ 
Containerd, on the other hand, focuses on the core functions of container management, such as starting and stopping containers, managing container images, and handling container lifecycle events.

- Compatibility: 
Containerd is designed to be compatible with a wide range of container management tools and orchestrators, making it a flexible choice for organizations that need to integrate with existing infrastructure. \ 
Docker, on the other hand, is optimized for use with Docker Swarm, Kubernetes, and other Docker-specific tools.

- Resource usage: 
Docker can be more resource-intensive than Containerd, especially in large-scale deployments. Docker provides a more comprehensive set of features and tools, but this can also make it more complex and resource-intensive than Containerd.

**dockerd vs docker-containerd vs docker-runc vs docker-containerd-ctr vs docker-containerd-shim**
- dockerd: The Docker daemon itself. The highest level component in your list and also the only 'Docker' product listed. Provides all the nice UX features of Docker.
- (docker-)containerd: Also a daemon, listening on a Unix socket, exposes gRPC endpoints. Handles all the low-level container management tasks, storage, image distribution, network attachment, etc...
- (docker-)containerd-ctr: A lightweight CLI to directly communicate with containerd. Think of it as how 'docker' is to 'dockerd'.
- (docker-)runc: A lightweight binary for actually running containers. Deals with the low-level interfacing with Linux capabilities like cgroups, namespaces, etc...
- (docker-)containerd-shim: After runC actually runs the container, it exits (allowing us to not have any long-running processes responsible for our containers). The shim is the component which sits between containerd and runc to facilitate this.
[MoreDetails](https://cto.ai/blog/overview-of-different-container-runtimes/)

**Kubernetes Pods**
- A Pod is the smallest and simplest unit in Kubernetes that can be deployed and managed.
- A Pod represents a single instance of a running process in a cluster, and can contain one or more containers that share the same network namespace, and can access the same storage volumes.
- Pods provide an abstraction layer for managing containerized applications in Kubernetes, and allow for flexible scaling and deployment of applications across a cluster.
- Pods can be created and managed using YAML configuration files or using command-line tools like kubectl.
- Pods can be scheduled to run on specific nodes in a cluster, and can be automatically rescheduled in the event of node failure or other issues.
- Pod-to-Pod communication within a cluster can be facilitated using Kubernetes Services, which provide a stable IP address and DNS name for a set of Pods.
- Pods are considered to be ephemeral and disposable, and are typically not directly accessed by end-users or applications. Instead, higher-level Kubernetes constructs like Deployments or StatefulSets are used to manage the lifecycle and scaling of Pods in a more automated and resilient way.
Here's an example of a basic Pod definition in Kubernetes using a YAML configuration file:
vim my-pod.yaml
```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
  labels:
    app: my-app
spec:
  containers:
    - name: my-container
      image: nginx:latest
      ports:
        - containerPort: 80
```
In this example, the Pod is defined with the following attributes:
- apiVersion: specifies the Kubernetes API version to use for this resource.
- kind: specifies the type of resource being defined, in this case a Pod.
- metadata: contains metadata about the Pod, including the name and any labels.
- spec: defines the specification for the Pod, including any containers that should be run inside the Pod.
- containers: specifies the list of containers to run inside the Pod, in this case a single container named my-container running the nginx:latest image.
- ports: specifies the container port(s) to expose for the container(s) running inside the Pod, in this case port 80.

```
kubectl apply -f my-pod.yaml
kubectl get pods
kubectl describe pod <pod-name>
```

### Pod Lab
1. How many pods exist on the system? \
`kubectl get pods`
2. Create a new pod with the nginx image. \
`kubectl run nginx --image=nginx`
3. How many pods are created now? \
`kubectl get pods`
4. What is the image used to create the new pods? \
`kubectl describe pod newpods-<id>`
5. Which nodes are these pods placed on?
```
kubectl get pods -o wide
kubectl describe pod newpods-mw5w2
#Node:controlplane/172.25.0.3
```
6. How many containers are part of the pod webapp? \
`kubectl get pods`
7. What is the state of the container agentx in the pod webapp? \
`kubectl describe pod webapp`
8. What does the READY column in the output of the kubectl get pods command indicate? \
`first number indicates the number of containers that are currently running and available, and the second number indicates the total number of containers in the Pod.`
9. Create a new pod with the name redis and with the image redis123. Use a pod-definition YAML file. And yes the image name is wron.
```
kubectl run redis --image=redis123 --dry-run=client -o yaml > redis-definition.yaml
kubectl create -f redis-definition.yaml
kubectl get pods
```
10. Now change the image on this pod to redis. Once done, the pod should be in a running state.
```
kubectl edit pod redis
kubectl apply -f redis-definition.yaml 
```
### ReplicationController and ReplicaSet!!!
ReplicationController and ReplicaSet are two Kubernetes controllers that are used to ensure that a specified number of Pod replicas are running and available at all times. They are used to provide high availability and fault tolerance for applications running in Kubernetes.

ReplicationController was the first controller that was introduced in Kubernetes to manage the lifecycle of Pods. It ensures that a specified number of Pod replicas are running at all times, and can automatically create or delete Pods as needed to maintain the desired number of replicas. However, ReplicationController has limitations, such as the inability to perform rolling updates, and is therefore considered to be a deprecated feature.

ReplicaSet is an improvement over ReplicationController, and is the recommended way to manage replicas of Pods in Kubernetes. ReplicaSet is similar to ReplicationController, but provides more powerful and flexible features, such as the ability to perform rolling updates and to use more expressive Pod selectors. ReplicaSets also support the use of more advanced Pod templates, and can manage Pods with more complex configurations, such as multi-container Pods.

In summary, both ReplicationController and ReplicaSet are used to manage the lifecycle of Pod replicas in Kubernetes, but ReplicaSet is the recommended way to do so due to its more advanced and flexible features. If you are working with an older version of Kubernetes or an application that still uses ReplicationController, it is recommended to upgrade to ReplicaSet for better manageability and reliability.

**ReplicaSet Lab**
1. How many ReplicaSets exist on the system? \
`kubectl get replicaset`

2. How many PODs are DESIRED in the new-replica-set? \
Run the command: kubectl get replicaset and look at the count under the Desired column. \
`kubectl get replicaset`

3. What is the image used to create the pods in the new-replica-set? \
`kubectl describe rs new-replica-set`

4. Why do you think the PODs are not ready?
```
kubectl get pod
kubectl describe pod new-replica-set-ngv9g
```
5. Delete any one of the 4 PODs. \
`kubectl  delete rs new-replica-set`

6. How to find correct the apiVersion for ReplicaSet. \
`kubectl explain replicaset | grep VERSION`

7. Delete the two newly created ReplicaSets - replicaset-1 and replicaset-2 \
`kubectl delete rs replicaset-1 replicaset-2`

8. Scale the ReplicaSet to 5 PODs, Use kubectl scale command or edit the replicaset using kubectl edit replicaset.
```
kubectl edit replicaset new-replica-set
kubectl scale rs new-replica-set --replicas=5
```
9. Now scale the ReplicaSet down to 2 PODs, Use the kubectl scale command.

`kubectl scale rs new-replica-set --replicas=2`
\
### Deployment
**What is Deployment!!!** \
Deployments are a higher-level Kubernetes object that provide a declarative way to manage your application's lifecycle. A Deployment ensures that a specified number of replicas of your application are running at all times, and allows you to perform rolling updates and rollbacks without downtime.

The main components of a Deployment are:
- ReplicaSet: The ReplicaSet is responsible for maintaining a desired number of replicas of your application. It is created and managed by the Deployment controller. The ReplicaSet uses a Pod template to create new replicas of your application, and automatically scales up or down as needed to maintain the desired number of replicas.
- Pod template: The Pod template is a specification for the Pods that will be created by the ReplicaSet. It includes information about the container image, container ports, and environment variables for your application.
- Deployment strategy: The Deployment strategy determines how updates to your application are rolled out. By default, a Deployment uses a "RollingUpdate" strategy, which gradually replaces old replicas with new ones. However, you can also use a "Recreate" strategy, which deletes all the old replicas before creating new ones.

**Deployments offer several benefits over using ReplicaSets directly**

1. Rolling updates and rollbacks: With a Deployment, you can update your application without downtime by using a rolling update strategy. The Deployment controller will gradually update the replicas of your application, replacing old replicas with new ones one by one. If any problems occur during the update, you can quickly roll back to the previous version. This is useful for keeping your application up-to-date and secure.

2. Scaling and self-healing: Deployments automatically create and manage ReplicaSets for you, ensuring that a specified number of replicas of your application are running at all times. If a Pod fails, the Deployment controller will automatically create a new replica to replace it. This helps ensure that your application is always available to users.

3. Versioning and history: Deployments keep a record of all revisions to your application, including the Pod template, the image used, and the rollout history. This allows you to easily roll back to a previous version of your application if necessary.

4. Easy to use: Deployments provide a simple and declarative way to manage your application's lifecycle. You can specify the desired state of your application in a YAML file and apply it using the kubectl apply -f command. The Deployment controller will automatically create and manage the necessary ReplicaSets and Pods for you.
\
**Deployments Lab** \
How many Deployments exist on the system? \
`kubectl get deployment` \
What is the image used to create the pods in the new deployment?
```
kubectl get deploy
kubectl describe deploy frontend-deployment
```

The value for kind is incorrect. It should be Deployment \
`kubectl explain deployment | head -n1`

Create a new Deployment with the below attributes using your own deployment definition file.Name: httpd-frontend; Replicas: 3; Image: httpd:2.4-alpinei \
`kubectl create deployment httpd-frontend --image=httpd:2.4-alpine --replicas=3`

### Kubernetes Namespaces

**What is Kubernetes Namespaces!!!**
Namespaces are a way to organize and group objects in a Kubernetes cluster. They provide a scope for Kubernetes resources, and allow different teams or projects to use the same cluster without interfering with each other.
Each Kubernetes object belongs to a namespace, unless it is explicitly declared to be in the global namespace. When you create a new object, you can specify which namespace it should be created in.

Namespaces provide the following benefits: 
1. Resource isolation: Namespaces provide a way to isolate resources and prevent them from conflicting with each other. For example, you can have multiple teams or projects using the same cluster, but each team or project can have its own namespace, and objects in one namespace can't interfere with objects in another namespace.

2. Resource sharing: Namespaces allow resources to be shared across different teams or projects. For example, you can create a common namespace for shared resources like databases or storage, and other teams or projects can use those resources by referencing them in their own namespaces.

3. Access control: Namespaces provide a way to control access to resources. You can define Role-Based Access Control (RBAC) policies for each namespace, allowing you to control who can access which resources in that namespace.

4. Resource quota: Namespaces can have resource quotas defined on them, which limit the amount of resources that can be used by objects in that namespace. This helps prevent resource exhaustion and ensures that each team or project has access to the resources it needs.

Overall, namespaces provide a powerful way to organize and manage resources in a Kubernetes cluster, allowing you to isolate and share resources as needed, control access to those resources, and ensure that each team or project has access to the resources it needs.

**DNS service in Kubernetes** \
When a Pod is created in Kubernetes, it gets assigned a unique IP address, which is used to identify and communicate with the Pod. In addition to the IP address, Kubernetes also assigns a DNS name to each Pod, which allows other Pods and services to easily locate and communicate with it. \
\
The DNS name of a Pod consists of the Pod's name and the namespace it is in, in the format <pod-name>.<namespace>.pod.cluster.local. For example, if you have a Pod named my-pod in a namespace named my-namespace, its DNS name would be my-pod.my-namespace.pod.cluster.local. \
\
This DNS name is automatically registered with the Kubernetes DNS service, which allows other Pods and services within the cluster to discover and communicate with the Pod using its DNS name. When a Pod is deleted, its DNS name is automatically removed from the DNS service. \
\
Kubernetes assigns a DNS name to each Service in a similar format as the DNS name for Pods. The DNS name of a Service consists of the Service name and the namespace it is in, in the format <service-name>.<namespace>.svc.cluster.local.


**Namespace Lab** 

How many Namespaces exist on the system? \
`kubectl get ns --no-headers | wc -l`

How many pods exist in the research namespace? \
`kubectl get pod -n research`

Create a POD in the finance namespace. Use the spec, Name: redis, Image Name: redis \
`kubectl run redis --image=redis -n finance`

Which namespace has the blue pod in it? \
`kubectl get pods --all-namespaces | grep blue`
\


**Summary Lab of Kubernetes Core Concept** 

Deploy a pod named nginx-pod using the nginx:alpine image. \
`kubectl run nginx-pod --image=nginx:alpine`

Deploy a redis pod using the redis:alpine image with the labels set to tier=db. \
`kubectl run redis --image=redis:alpine --labels tier=db`

Create a service redis-service to expose the redis application within the cluster on port 6379. \
`kubectl expose pod redis --port=6379 --name=redis-service`

Create a deployment named webapp using the image kodekloud/webapp-color with 3 replicas. \
`kubectl create deployment webapp --image=kodekloud/webapp-color --replicas=3`

Create a new pod called custom-nginx using the nginx image and expose it on container port 8080 \
`kubectl run custom-nginx --image=nginx --port=8080`

Create a new namespace called dev-ns. \
`kubectl create namespace dev-ns`

Create a new deployment called redis-deploy in the dev-ns namespace with the redis image. It should have 2 replicas. \
`kubectl create deployment redis-deploy --image=redis --replicas=2 -n dev-ns`

Create a pod called httpd using the image httpd:alpine in the default namespace. Next, create a service of type ClusterIP by the same name (httpd). The target port for the service should be 80. \
```
kubectl run httpd --image=httpd:alpine
kubectl expose pod httpd --port=80 --type=ClusterIP
or
kubectl run httpd --image=httpd:alpine --port=80 --expose
```

## Configuration!!

**Commands and Arguments in Docker!!!** \
Commands in Docker are actions that can be performed on containers and images. 
Commands example: \
docker run: This command creates and starts a container from an image. \
docker stop: This command stops a running container. \
docker start: This command starts a stopped container. \
docker ps: This command lists all running containers. \
docker images: This command lists all available images on the local machine. \
docker pull: This command pulls an image from a registry or repository. \
docker push: This command pushes an image to a registry or repository. \
docker build: This command builds an image from a Dockerfile. 


Arguments in Docker are options that modify the behavior of a command.
Arguments example: \
-d: This argument runs a container in detached mode, which means it runs in the background and doesn't attach to the terminal. \
-p: This argument maps a container port to a host port. \
-e: This argument sets environment variables for a container. \
-v: This argument mounts a volume from the host machine to the container. \
-name: This argument assigns a name to a container. \
-rm: This argument automatically removes a container when it stops running. \
-t: This argument assigns a pseudo-TTY terminal to a container. \
-i: This argument keeps STDIN open even if not attached to a terminal.


To manage the resources and applications in a Kubernetes cluster, developers and operators use a set of commands and arguments. 
Example of a Kubernetes Pod manifest file with commands and arguments.
```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: nginx-container
    image: nginx
    ports:
    - containerPort: 80
    #Command is ENTRYPOINT
    command: ["/bin/bash"]
    #Args is CMD
    args: ["-c", "echo 'Hello, world!' && sleep 3600"]
```

The command field specifies the command that should be executed when the container starts. In this case, we're using /bin/bash as the command, which is the shell used by many Unix-based operating systems. \

The args field specifies the arguments that should be passed to the command. In this case, we're using -c to tell Bash to execute a single command, and then we're passing in a string that includes the echo command to print "Hello, world!" and the sleep command to keep the container running for an hour.

**Commands and Arguments Lab**

1. What is the command used to run the pod ubuntu-sleeper?
```
kubectl get pod
kubectl describe pod ubuntu-sleeper
```
```
FROM python:3.6-alpine
RUN pip install flask
COPY . /opt/
EXPOSE 8080
WORKDIR /opt
ENTRYPOINT ["python", "app.py"]
```
```
FROM python:3.6-alpine
RUN pip install flask
COPY . /opt/
EXPOSE 8080
WORKDIR /opt
ENTRYPOINT ["python", "app.py"]
CMD ["--color", "red"]
```                       
This Dockerfile is used to build a Docker image for a Flask web application. Here's what each line does:
- FROM python:3.6-alpine: Specifies that the base image for the Docker image should be the python:3.6-alpine image, which is a lightweight image based on Alpine Linux that includes Python 3.6.
- RUN pip install flask: Installs the Flask Python package in the Docker image using the pip package manager.
- COPY . /opt/: Copies the contents of the current directory (i.e., the directory containing the Dockerfile and the Flask application code) into the /opt directory of the Docker image.
- EXPOSE 8080: Informs Docker that the container will listen on port 8080 at runtime.
- WORKDIR /opt: Sets the working directory to /opt.
- ENTRYPOINT ["python", "app.py"]: Specifies the command that should be run when the container starts. In this case, the python command is used to execute the app.py Flask application.
- CMD ["--color", "red"]: Provides default arguments to the command specified in the ENTRYPOINT directive. In this case, the default arguments are --color and red.
- When you build a Docker image from this Dockerfile and run a container based on that image, the Flask application will be available at http://localhost:8080.

2. Create a pod with the given specifications. By default it displays a blue background. Set the given command line arguments to change it to green. \
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: webapp-green
  name: webapp-green
spec:
  containers:
  - image: kodekloud/webapp-color
    name: webapp-green
    args: ["--color", "green"]
```
### Environment Variables
Environment variables in Kubernetes are used to pass configuration information to the containers running in a pod. They are commonly used to pass sensitive information like credentials, database connection strings, and API keys.

In Kubernetes, environment variables can be set in several ways:
1. In the pod definition file, using the env field
```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: my-image
    env:
    - name: MY_VAR
      value: my_value
```
This sets the environment variable MY_VAR to the value my_value in the container running the my-image image.

2. Using a ConfigMap
```
apiVersion: v1
kind: ConfigMap
metadata:
  name: my-config
data:
  MY_VAR: my_value
``` 
This creates a ConfigMap with the MY_VAR environment variable set to my_value. The ConfigMap can then be mounted into the container as environment variables using the envFrom field in the pod definition file.
```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: my-image
    envFrom:
    - configMapRef:
        name: my-config
```
To create a ConfigMap in Kubernetes using the imperative approach, you can use the kubectl create configmap command. Here's the basic syntax: \
`kubectl create configmap NAME --from-literal=KEY1=VALUE1 --from-literal=KEY2=VALUE2`

**configMap Lab**
1. What is the environment variable name set on the container in the pod? \
`kubectl describe pod webapp-color`

2. How many configmap exist in the default namespace. \
`kubectl get config map`

3. Identify the database host from the config map db-config. \
`kubectl describe configmaps db-config`

4. Create a new ConfigMap for the webapp-color POD. Use the spec given below.ConfigMap. \
Name: webapp-config-map.
Data: APP_COLOR=darkblue
Data: APP_OTHER=disregard

`kubectl create configmap webapp-config-map --from-literal=APP_COLOR=darkblue --from-literal=APP_OTHER=disregard`

5. Update the environment variable on the POD to use only the APP_COLOR key from the newly created ConfigMap.
Pod Name: webapp-color
ConfigMap Name: webapp-config-map

```
kubectl get pod
kubectl edit pod webapp-color
spec:
  containers:
  - env:
    - name: APP_COLOR
      valueFrom:
       configMapKeyRef:
         name: webapp-config-map
         key: APP_COLOR

kubectl delete pod webapp-color
kubectl apply -f /tmp/kubectl-edit-566315995.yaml
```

3. Using a Secret \
In Kubernetes, a Secret is an object that is used to store sensitive information, such as passwords, API keys, and certificates. Secrets are stored in etcd and can be mounted as files or environment variables in a pod's container.
Here's an example of how to create a Secret in Kubernetes using the imperative approach: \
`kubectl create secret generic my-secret --from-literal=PASSWORD=password123 --from-literal=API_KEY=apikey123` \
This creates a Secret named `my-secret` with two key-value pairs: `PASSWORD=password123` and `API_KEY=apikey123`
```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: my-image
    env:
    - name: PASSWORD
      valueFrom:
        secretKeyRef:
          name: my-secret
          key: PASSWORD
    - name: API_KEY
      valueFrom:
        secretKeyRef:
          name: my-secret
          key: API_KEY
```
This sets the `PASSWORD` and `API_KEY` environment variables in the container running the `my-image` image to the values stored in the `my-secret` Secret.
You can also create a Secret from a file using the --from-file flag \
`kubectl create secret generic my-secret --from-file=cert.pem --from-file=key.pem` \
This creates a Secret named `my-secret` with two key-value pairs, `cert.pem` and `key.pem`, that contain the contents of the corresponding files.
To use a Secret in a pod, you can add a `volumeMounts` field to the container spec to mount the Secret as a file.
```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: my-image
    volumeMounts:
    - name: my-secret
      mountPath: /etc/my-secret
      readOnly: true
  volumes:
  - name: my-secret
    secret:
      secretName: my-secret
```
This mounts the `my-secret` Secret as a read-only volume in the container running the `my-image` image. The contents of the Secret are available in the directory `/etc/my-secret`.

**Secret Lab**
1. How many secrets exist in the system? \
`kubectl get secrets`
2. How many secrets are defined in the dashboard-token secret? \
`kubectl describe secrets dashboard-token`
3. What is the type of the dashboard-token secret? \
`kubectl describe secrets dashboard-token`
4. The reason the application is failed is because we have not created the secrets yet. Create a new secret named db-secret with the data given below. \
    Secret Name: db-secret \
    Secret 1: DB_Host=sql01 \
    Secret 2: DB_User=root \
    Secret 3: DB_Password=password123 \
`kubectl create secret generic db-secret --from-literal=DB_Host=sql01 --from-literal=DB_Password=password123 --from-literal=DB_User=root`
5. Configure webapp-pod to load environment variables from the newly created secret.
Delete and recreate the pod if required. \
    Pod name: webapp-pod \
    Image name: kodekloud/simple-webapp-mysql \
    Env From: Secret=db-secret 

```
spec:
  containers:
  - image: kodekloud/simple-webapp-mysql
    imagePullPolicy: Always
    name: webapp
    envFrom:
    - secretRef:
        name: db-secret
```

### Encrypting Secret Data at Rest

By default, Kubernetes stores Secrets as base64-encoded strings, which are not encrypted at rest. To protect sensitive data, Kubernetes provides several mechanisms for encrypting Secret data at rest. 

One option is to use the Kubernetes Encryption Provider. This feature uses the native encryption features of the underlying cloud infrastructure to encrypt Secrets at rest. When you enable the Encryption Provider, Kubernetes automatically encrypts Secret data before storing it in etcd. To enable the Encryption Provider, you need to create a Kubernetes EncryptionConfig object that specifies the encryption provider and key configuration. Here's an example of an EncryptionConfig object that uses the AWS KMS encryption provider. \
[Referance](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/)


### Pre-requisite – Security in Docker
By default, the root user in a Docker container does not have direct access to the Docker host machine. However, it is possible for a skilled attacker to use a privilege escalation vulnerability in the container or a Docker API vulnerability to gain access to the host machine.
Therefore, it is important to ensure that your Docker environment is properly secured and that you follow best practices for container security. Some recommended security measures include:
1. Running containers with a non-root user whenever possible, to reduce the risk of privilege escalation attacks.
2. Using container orchestration tools like Kubernetes to enforce network isolation, resource limits, and security policies.
3. Limiting access to the Docker daemon and API using firewall rules and access controls.
4. Using TLS/SSL certificates to encrypt traffic between Docker components and the Docker registry.
5. Monitoring your Docker environment for security threats and vulnerabilities using tools like Docker Bench Security and logging and monitoring tools. \
In summary, while it is possible for a container root user to access the Docker host machine in certain circumstances, following best practices for container security can greatly reduce this risk.

Here is an example Dockerfile that creates a non-root user called appuser and runs the container as that user. 
```
FROM python:3.9-slim
# Create a non-root user
RUN adduser --disabled-password --gecos "" appuser
# Set the working directory
WORKDIR /app
# Copy the requirements file
COPY requirements.txt .
# Install the dependencies
RUN pip install --no-cache-dir -r requirements.txt
# Copy the application files
COPY . .
# Set the ownership of the application directory to the non-root user
RUN chown -R appuser:appuser /app
# Switch to the non-root user
USER appuser
# Start the application
CMD ["python", "app.py"]
```
In this Dockerfile, we first create a non-root user called appuser using the adduser command. We then set the working directory, copy the requirements.txt file, and install the dependencies. We also copy the application files and set the ownership of the application directory to the non-root user. \
Finally, we switch to the appuser using the USER command and start the application using the CMD command. When you build and run this Docker image, the container will run as the appuser rather than the root user.

### Kubernetes Security Contexts
Kubernetes Security Contexts are used to configure security-related attributes for pods and containers. They provide a way to enforce security policies and controls at the pod and container level, such as running containers as non-root users, setting read-only file systems, and limiting the amount of CPU and memory that a container can use.

There are two types of Security Contexts in Kubernetes:
1. Pod Security Contexts: Pod Security Contexts are used to configure security-related attributes for a pod as a whole. These attributes apply to all containers within the pod.
2. Container Security Contexts: Container Security Contexts are used to configure security-related attributes for a specific container within a pod.
Here's an example of a Pod Security Context and a Container Security Context.
```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  securityContext:
    runAsUser: 1000
    fsGroup: 2000
  containers:
  - name: my-container
    image: my-image
    securityContext:
      allowPrivilegeEscalation: false
      readOnlyRootFilesystem: true
```
In this example, the Pod Security Context is used to specify that the pod should be run as the user with UID 1000, and that the file system should be group-owned by the group with GID 2000. The Container Security Context is used to specify that the container should not allow privilege escalation and should have a read-only root file system. \
Overall, Security Contexts in Kubernetes provide an additional layer of security to help protect your applications and data.

### Resource requirements in Kubernetes!!
Resource requirements in Kubernetes allow you to specify the amount of CPU and memory resources that a container or pod requires to run properly. These requirements help Kubernetes scheduler make informed decisions about where to place and schedule the containers.

To define resource requirements, you can use the resources field in the pod or container specification. Here's an example:
```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: my-image
    resources:
      requests:
        cpu: "0.5"
        memory: "512Mi"
      limits:
        cpu: "1"
        memory: "1Gi"
```
In this example, the requests section specifies the minimum required resources for the container, while the limits section defines the maximum amount of resources the container is allowed to consume.

**CPU cores and millicores in Kubernetes, Example**

1. Requesting 0.5 CPU core
requests.cpu: "500m" (500 millicores) or requests.cpu: "0.5" (0.5 CPU core).This specifies that the container requires half of a CPU core for its operation.

2. Requesting 2 CPU cores requests.cpu: "2000m" (2000 millicores) or requests.cpu: "2" (2 CPU cores). This indicates that the container requires two CPU cores to run properly.

3. Limiting CPU usage to 1.5 CPU cores limits.cpu: "1500m" (1500 millicores) or limits.cpu: "1.5" (1.5 CPU cores). This sets the maximum limit for the container's CPU usage to 1.5 CPU cores.

**Memory resources are specified in Kubernetes, Example**

1. Requesting 512 mebibytes of memory requests.memory: "512Mi". This indicates that the container requires a minimum of 512 mebibytes (512 MiB) of memory to run properly.

2. Requesting 1 gibibyte of memory requests.memory: "1Gi" This specifies that the container requires at least 1 gibibyte (1 GiB) of memory.

3. Limiting memory usage to 2 gibibytes limits.memory: "2Gi" This sets the maximum limit for the container's memory usage to 2 gibibytes (2 GiB).


**Pod resources Limit Lab**
1. A pod called rabbit is deployed. Identify the CPU requirements set on the Pod? \
`kubectl describe pod rabbit`
2. Delete the rabbit Pod. \
`kubectl delete pod rabbit --force`
3. Another pod called elephant has been deployed in the default namespace. It fails to get to a running state. Inspect this pod and identify the Reason why it is not running. \
`kubectl describe pod elephant  | grep -A5 State:`
4. The status OOMKilled indicates that it is failing because the pod ran out of memory. Identify the memory limit set on the POD.

### Kubernetes Service Account
In Kubernetes, a Service Account is an identity associated with a pod that allows the pod to interact with the Kubernetes API and other resources within the cluster. Service Accounts are used to authenticate and authorize the requests made by pods to access various resources.

Here are some key points about Service Accounts in Kubernetes:

- Default Service Account: Every namespace in Kubernetes has a default Service Account named default. If a pod doesn't specify a Service Account, it is automatically assigned the default Service Account of its namespace.

- Authentication and Authorization: Service Accounts are primarily used for authentication and authorization purposes. When a pod makes API requests to the Kubernetes API server, it can present the credentials associated with its Service Account to prove its identity and permissions.

- Access Control: Service Accounts are tied to a set of Kubernetes Role-Based Access Control (RBAC) rules. These rules define the permissions and access levels that a Service Account has within the cluster. By default, Service Accounts have limited permissions, but you can create custom RBAC rules to grant additional privileges if needed.

- Token-based Authentication: Each Service Account is associated with a unique token that is automatically mounted into the pod's filesystem. The pod can use this token to authenticate itself when communicating with the Kubernetes API server.

- Service Account Secrets: The token and other associated information for a Service Account are stored in a secret object within the same namespace. This secret is automatically created and managed by Kubernetes.

- Custom Service Accounts: Besides the default Service Account, you can create custom Service Accounts to grant specific permissions to pods. This allows you to have fine-grained control over the access rights of different pods within your cluster.

Service Accounts are an essential component of securing and managing access to resources within a Kubernetes cluster. They provide a way to authenticate and authorize pods, ensuring that only authorized entities can interact with the Kubernetes API and perform actions on resources.


**Service Account Lab** \
How many Service Accounts exist in the default namespace? \
`kubectl get sa`

What is the secret token used by the default service account? \
`kubectl describe serviceaccount default`

We just deployed the Dashboard application. Inspect the deployment. What is the image used by the deployment? \
```
kubectl get deployments
kubectl describe deploy web-dashboard
```
Inspect the Dashboard Application POD and identify the Service Account mounted on it. 
```
kubectl get pod
kubectl get pod web-dashboard-65b9cf6cbb-9hgbl -o yaml
```
At what location is the ServiceAccount credentials available within the pod? 
```
kubectl get pod
kubectl get pod web-dashboard-65b9cf6cbb-9hgbl -o yaml
```
Create a new ServiceAccount named dashboard-sa. \
`kubectl create sa dashboard-sa`

Create an authorization token for the newly created service account, copy the generated token and paste it into the token field. \
`kubectl create token dashboard-sa`

```
kubectl create sa dashboard-sa
kubectl describe secret token

```

**Kubernetes Taints and Tolerations** \
In Kubernetes, taints and tolerations are used to control the scheduling and placement of pods on specific nodes. They allow you to specify node preferences and constraints, ensuring that pods are scheduled on suitable nodes based on certain conditions.
Here's an overview of taints and tolerations:
1. Taints: A taint is a property assigned to a node that indicates it has a certain constraint or preference. Taints are used to repel pods from being scheduled on nodes that don't meet specific criteria. Nodes can have multiple taints, each with a key, value, and effect. The "effect" of a taint can be one of the following:
- NoSchedule: Pods without a matching toleration won't be scheduled on nodes with this taint.
- PreferNoSchedule: Scheduler will try to avoid placing pods without a matching toleration on nodes with this taint but can still schedule if necessary.
- NoExecute: Any pod without a matching toleration will be evicted from nodes with this taint.

2. Tolerations: A toleration is a property specified in a pod's configuration that allows it to tolerate or accept certain taints on nodes. olerations are used to override the taint-based restrictions and allow pods to be scheduled on specific nodes with matching taints. Pods can have multiple tolerations, each with a key, value, operator, and effect. The "effect" of a toleration must match the effect of the corresponding taint.
The "operator" can be one of the following:
- Exists: Matches any taint with the specified key.
- Equal: Matches taints with the specified key and value.

By using taints and tolerations, you can influence the pod scheduling process in Kubernetes. It provides control over pod placement,segregation of workloads, and enables various use cases such as dedicating nodes to specific tasks or marking nodes for maintenance.

Taints and tolerations help ensure that pods are deployed on appropriate nodes based on the specified constraints, enhancing the stability and efficiency of your Kubernetes cluster.

Here's an example that demonstrates the usage of taints and tolerations in Kubernetes:
Let's assume we have a cluster with three nodes, and we want to taint one of the nodes to repel certain pods from being scheduled onit.
1. Taint a Node: Taint the node named "node1" with the key "special" and value "true" using the kubectl taint command. \ 
`kubectl taint nodes node1 special=true:NoSchedule`

2. Create a Pod with a Matching Toleration: Create a pod definition file, let's name it "my-pod.yaml", with the following content.  \
```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: nginx
tolerations:
- key: "special"
  operator: "Equal"
  value: "true"
  effect: "NoSchedule"

```
In this example, we taint "node1" with the "special=true:NoSchedule" taint, which means pods without a matching toleration won't be scheduled on this node.

The pod definition file specifies a toleration with the same key ("special"), value ("true"), operator ("Equal"), and effect ("NoSchedule") as the taint on "node1". This allows the pod to tolerate the taint and be scheduled on the tainted node.

By applying the pod definition, the pod will be scheduled on "node1" despite its taint, thanks to the matching toleration.

This example demonstrates how taints and tolerations work together to control pod scheduling based on specific node constraints. You can customize the taints and tolerations according to your specific requirements to achieve desired pod placement behavior within your Kubernetes cluster.


**Taints and Tolerations Lab**
1. How many nodes exist on the system? \
`kubectl get nodes`

2. Do any taints exist on node01 node? \
`kubectl describe nodes node01 | grep -i taints`

3. Create a taint on node01 with key of spray, value of mortein and effect of NoSchedule \
Key = spray, Value = mortein, Effect = NoSchedule \
`kubectl taint nodes node01 spray=mortein:NoSchedule`

4. Create a new pod with the nginx image and pod name as mosquito. \
`kubectl run mosquito --image=nginx`

5. What is the pos state \
`kubectl get pod`

6. Create another pod named bee with the nginx image, which has a toleration set to the taint mortein
`kubectl delete pod mosquito --force`
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: bee
  name: bee
spec:
  containers:
  - image: nginx
    name: bee
  tolerations:
  - key: "spray"
    value: "mortein"
    operator: "Equal"
    effect: "NoSchedule"
```
7. Do you see any taints on controlplane node? \
`ubectl describe nodes controlplane | grep -i taints`

8. Remove the taint on controlplane, which currently has the taint effect of NoSchedule. \
`kubectl taint node controlplane node-role.kubernetes.io/control-plane:NoSchedule-`

**Node Selectors** \
In Kubernetes, node selectors are used to schedule pods on specific nodes based on labels assigned to the nodes. By using node selectors, you can control which nodes are eligible for running certain pods. This allows you to distribute workload and allocate resources according to your requirements.

Here's an example of how to use node selectors in Kubernetes:

1. Label Nodes: Start by labeling the nodes in your cluster. For example, let's label a node named "node1" with a key-value pair of "app: backend" and label another node named "node2" with a key-value pair of "app: frontend". Use the kubectl command to label the nodes:
```
kubectl label nodes node1 app=backend
kubectl label nodes node2 app=frontend
```

2. Create Pod with Node Selector: Create a pod definition file, let's name it "my-pod.yaml", with the following content:
```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: nginx
  nodeSelector:
    app: backend
```
In this example, we labeled "node1" with the key-value pair of "app: backend" and "node2" with "app: frontend". The pod definition file specifies a node selector with the key-value pair of "app: backend". This ensures that the pod will be scheduled only on nodes with the label "app: backend"

**Node Affinity**

Node affinity is a Kubernetes feature that allows you to schedule pods on nodes based on custom rules and conditions. It provides more advanced options for pod placement compared to simple node selectors.

There are two types of node affinity in Kubernetes:

1. RequiredDuringSchedulingIgnoredDuringExecution:
   - This type of node affinity specifies rules that must be satisfied for a pod to be scheduled on a node.
   - If the rules are not met, the pod will not be scheduled on any node.
   - An example of using required node affinity could be to schedule a pod only on nodes with a specific label.

2. PreferredDuringSchedulingIgnoredDuringExecution:
   - This type of node affinity specifies preferences for pod scheduling.
   - The rules are not mandatory, but if they are met, the pod will be scheduled on a node that satisfies the preferences.
   - If multiple preferred rules are specified, the one with the highest priority will be used.
   - An example of using preferred node affinity could be to schedule a pod on nodes that have a certain label, but if none of those nodes are available, the pod can be scheduled on other nodes. \
Here's another example that demonstrates the usage of preferred node affinity in Kubernetes.

```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: nginx
  affinity:
    nodeAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 100
        preference:
          matchExpressions:
          - key: environment
            operator: In
            values:
            - production
      - weight: 50
        preference:
          matchExpressions:
          - key: app
            operator: In
            values:
            - backend
```
In this example, the pod has preferred node affinity rules. It specifies two preferences for pod scheduling:
1. The first preference has a weight of 100 and specifies that the pod should be scheduled on nodes with the label "environment" having the value "production". This is a strong preference, and if there are nodes that satisfy this rule, the pod will be scheduled on one of those nodes.
2. The second preference has a weight of 50 and specifies that the pod should be scheduled on nodes with the label "app" having the value "backend". This is a weaker preference compared to the first preference. If there are no nodes that satisfy the first preference, the pod will be scheduled on nodes that satisfy this preference.

The weight field defines the priority of the preference. If multiple preferred rules are specified, the one with the highest weight will be given priority.

By using preferred node affinity, you can define flexible scheduling rules and preferences for your pods. This allows for better utilization of resources and the ability to prioritize certain node characteristics based on your application's requirements.


**Node Affinity Lab**
1. How many Labels exist on node node01? \
`kubectl get node node01 --show-labels`

2. Apply a label color=blue to node node01 \
`kubectl label nodes node01 color=blue`

3. Create a new deployment named blue with the nginx image and 3 replicas \
`kubectl create deploy blue --image=nginx --replicas 3`

4. Which nodes can the pods for the blue deployment be placed on? \
`kubectl get pod -o wide`

5. Set Node Affinity to the deployment to place the pods on node01 only. \
Name: blue, Replicas: 3, Image: nginx, NodeAffinity: requiredDuringSchedulingIgnoredDuringExecution, Key: color, value: blue \
`kubectl edit deploy blue `
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: blue
spec:
  replicas: 3
  selector:
    matchLabels:
      run: nginx
  template:
    metadata:
      labels:
        run: nginx
    spec:
      containers:
      - image: nginx
        imagePullPolicy: Always
        name: nginx
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: color
                operator: In
                values:
                - blue
```
[Referance](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity)

6. Create a new deployment named red with the nginx image and 2 replicas, and ensure it gets placed on the controlplane node only.
Use the label key - node-role.kubernetes.io/control-plane - which is already set on the controlplane node.
Name: red, Replicas: 2, Image: nginx, NodeAffinity: requiredDuringSchedulingIgnoredDuringExecution, Key: node-role.kubernetes.io/control-plane, Use the right operator. \
`kubectl create deployment red --image nginx --replicas 2 --dry-run=client -o yaml`
```
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: red
  name: red
spec:
  replicas: 2
  selector:
    matchLabels:
      app: red
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: red
    spec:
      affinity:
        nodeAffinity:
         requiredDuringSchedulingIgnoredDuringExecution:
           nodeSelectorTerms:
            - matchExpressions:
              - key: node-role.kubernetes.io/control-plane
                operator: Exists
      containers:
      - image: nginx
        name: nginx
```
**Comparison between Taints & Tolerations and Node Affinity** \
1. Taints & Tolerations:
- Taints and Tolerations are mechanisms used to influence pod scheduling in Kubernetes.
- Taints are applied to nodes to mark them with specific preferences or restrictions.
- Tolerations are set on pods, allowing them to tolerate specific taints on nodes.
- Taints are used to repel or repurpose nodes, while Tolerations allow pods to override the node's taints and get scheduled on the tainted nodes.
- Taints & Tolerations provide a way to add constraints to the scheduling process based on node characteristics, such as hardware requirements or special roles.

2. Node Affinity:
- Node Affinity is a mechanism used to express preferences for pod placement based on node attributes or labels.
- It allows you to define rules that determine where pods should be scheduled based on matching criteria specified by labels and expressions.
- Node Affinity provides more fine-grained control over pod placement by specifying node selectors, preferred and required node affinity rules, and anti-affinity rules.
- With Node Affinity, you can control pod placement based on node properties like region, availability zone, hardware capabilities, or other custom labels assigned to nodes.

*Key differences*
- Taints & Tolerations operate at the node level, influencing which pods can be scheduled on specific nodes.
- Node Affinity operates at the pod level, determining where pods should be scheduled based on node attributes or labels.
- Taints & Tolerations are useful for repelling or reserving nodes for specific workloads or scenarios, while Node Affinity allows for more precise control over pod placement based on node properties.
- Taints & Tolerations focus on the node's perspective, while Node Affinity focuses on the pod's perspective.

Overall, Taints & Tolerations and Node Affinity are two different mechanisms that can be used in combination or independently to control pod scheduling in Kubernetes, depending on your specific requirements and use cases.


## Multi-Container Pods 

In Kubernetes, multi-container pods refer to a design pattern where multiple containers are deployed together within a single pod. These containers share the same lifecycle, networking, and storage resources, allowing them to work closely together and communicate efficiently.
Here are some key aspects and benefits of using multi-container pods: 
1. Shared Resources: Containers within a pod can share the same network namespace, storage volumes, and access to local inter-process communication (IPC). This enables them to efficiently communicate and share data without the need for complex networking configurations or additional overhead.
2. Co-Located Services: Multi-container pods are commonly used to co-locate related services or components that work together to fulfill a specific task or functionality. For example, a web server container and a sidecar container for log forwarding can be deployed together in a pod to handle incoming requests and manage logging.
3. Inter-Container Communication: Containers within a pod can communicate with each other using localhost or shared volumes. This allows them to exchange data or coordinate actions easily, facilitating the development of tightly coupled applications.
4. Resource Sharing and Efficiency: Containers within the same pod share the same node resources, such as CPU and memory. This can lead to more efficient resource utilization since containers can scale together and avoid resource fragmentation.
5. Simplified Deployment and Management: Managing multiple related containers within a single pod simplifies deployment and maintenance tasks. For example, scaling or updating a multi-container pod can be performed as a single unit, reducing complexity and improving manageability.
6. Sidecar Pattern: The sidecar pattern is a common use case for multi-container pods, where a main application container is paired with a supporting container (sidecar) that provides additional functionality. This pattern enables the separation of concerns and promotes modular and scalable architectures. \
\
It's important to note that multi-container pods are not always necessary or appropriate for every application. They are best suited for scenarios where containers within the pod have a close relationship and need to work together closely. Care should be taken to ensure proper coordination, resource allocation, and container isolation within the shared pod environment. \
In Kubernetes, you can define multi-container pods in a YAML manifest file using the spec.containers field to specify multiple containers within the same pod. \
Let's take an example of a multi-container pod that follows the sidecar pattern. We'll create a pod with a main application container and a sidecar container for log collection and forwarding. \
```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
spec:
  containers:
    - name: main-app
      image: myapp-image:latest
      # Add the configuration for the main application container
    - name: sidecar-logs
      image: log-forwarder-image:latest
      # Add the configuration for the sidecar container responsible for log collection and forwarding
```
**Multi Container Pods Lab**
1. Identify the number of containers created in the red pod. \
`kubectl get pod`

2. Identify the name of the containers running in the blue pod. \
`kubectl describe pod blue`

3. Create a multi-container pod with 2 containers. \
Use the spec given below. If the pod goes into the crashloopbackoff then add the command `sleep 1000` in the lemon container.
`kubectl run yellow --image busybox --command sleep 1000 --dry-run=client -o yaml`
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: yellow
  name: yellow
spec:
  containers:
  - command:
    - sleep
    - "1000"
    image: busybox
    name: lemon

  - name: gold
    image: redis
```

4. The application outputs logs to the file /log/app.log. View the logs and try to identify the user having issues with Login. \
`kubectl -n elastic-stack exec -it app -- cat /log/app.log`

**Init Containers** \
In Kubernetes, initContainers are a feature that allows you to run one or more additional containers in a pod before the main containers start. InitContainers are primarily used to perform initialization tasks or setup actions required by the main application containers. They run to completion before the main containers start and share the same lifecycle and resources within the pod. \
Here are some key aspects and benefits of using initContainers. \
1. Initialization and Setup: InitContainers are used to perform tasks necessary for the successful initialization of the pod. This can include tasks like database schema creation, downloading configuration files, or setting up shared volumes.
2. Dependency Handling: InitContainers help manage dependencies by ensuring that certain prerequisites or dependencies are met before the main application containers start. For example, you can have an initContainer that waits for a database service to be ready before starting the application container that relies on that database.
3. Separate Execution Environment: InitContainers have their own execution environment, separate from the main containers in the pod. They can have different resource requirements, image versions, or even different Linux distributions.
4. Ordered Execution: InitContainers within a pod are executed sequentially in the order they are defined in the pod specification. This allows you to control the sequence of tasks and enforce dependencies between them.
5. Shared Resources: InitContainers share the same network namespace and volumes as the main containers within the pod. They can access and modify shared volumes or use networking to communicate with external resources. \
Here's an example of using an initContainer with a volume and volume mount in a pod specification for a log server scenario. 
```
apiVersion: v1
kind: Pod
metadata:
  name: log-server-pod
spec:
  initContainers:
    - name: init-setup
      image: busybox:1.33.1
      command: ["sh", "-c", "touch /logs/app.log"]
      volumeMounts:
        - name: log-volume
          mountPath: /logs
  containers:
    - name: log-server
      image: log-server-image:latest
      volumeMounts:
        - name: log-volume
          mountPath: /logs
  volumes:
    - name: log-volume
      emptyDir: {}
```
In this example:
1. The pod "log-server-pod" consists of an initContainer named "init-setup" and a log server container named "log-server".
2. The initContainer uses the busybox:1.33.1 image to perform the initialization task.
3. The initContainer runs a command to create an empty log file named "app.log" using the touch command in the /logs directory.
4. The volumeMounts section in the initContainer specifies a mount path /logs and associates it with the log-volume.
5. The log server container uses the log-server-image:latest image and also mounts the log-volume at the /logs directory.
6. The volumes section defines the log-volume as an emptyDir volume to provide temporary storage within the pod. \
During the pod's startup, the initContainer "init-setup" runs and creates an empty log file named "app.log" within the shared volume at /logs. Once the initContainer completes successfully, the log server container "log-server" starts and can access the shared volume to write log data. \
You can customize this example further to fit your specific log server requirements, such as using a different log server image or configuring log rotation. Additionally, you may consider using a persistent volume (PV) and persistent volume claim (PVC) instead of an emptyDir volume if you need to retain log data even if the pod restarts.

**initContainer Lab**

1. Identify the pod that has an initContainer configured. \
`kubectl describe pod blue`

2. What is the image used by the initContainer on the blue pod? \
`kubectl describe pod blue`

3. What is the state of the initContainer on pod blue? \
`kubectl describe pod blue`

4. Why is the initContainer terminated? What is the reason? \
Check the Reason field for the initContainer \
`kubectl describe pod blue`


## Observability in the Kubernetes!!

Observability in the context of Kubernetes refers to the ability to gain insights into the internal state and behavior of a Kubernetes cluster, applications, and infrastructure components. It involves monitoring, logging, and tracing to collect data and provide visibility into the system's performance, health, and behavior.

**Readiness probes** 
In Kubernetes, readiness probes are a mechanism used to determine if a container within a pod is ready to accept traffic. Readiness probes are defined in the pod specification and are used by Kubernetes to determine whether or not a pod is ready to serve requests. \
The readiness probe can be configured with different types, such as HTTP, TCP, or command probes. Here are the three types of readiness probes commonly used. \
HTTP Readiness Probe: This type of probe performs an HTTP GET request to a specified endpoint on the container and checks for a successful response code (2xx or 3xx). If the response code indicates success, the container is considered ready. If the probe fails (e.g., non-2xx or 3xx response code or a connection timeout), the container is marked as not ready. An example of an HTTP readiness probe configuration:
```
readinessProbe:
  httpGet:
    path: /healthz
    port: 8080
  initialDelaySeconds: 5
  periodSeconds: 10
```
In this example, the readiness probe performs an HTTP GET request to /healthz on port 8080. The probe starts 5 seconds after the container starts and runs every 10 seconds thereafter.

**Liveness Probes** \
Liveness probes in Kubernetes are used to determine the health of a container within a pod. They are configured in the pod specification and are used by Kubernetes to determine whether a container is still running correctly. If a liveness probe fails, Kubernetes will restart the container automatically. \
Liveness probes can be configured with different types: HTTP, TCP, or command probes. Here's a breakdown of each type. \
HTTP Liveness Probe: This type of probe performs an HTTP GET request to a specific endpoint on the container and checks for a successful response code. If the response code indicates success (2xx or 3xx), the container is considered healthy. If the probe fails (e.g., non-2xx or 3xx response code, a connection timeout, or no response), the container is marked as unhealthy, and Kubernetes will restart it. An example of an HTTP liveness probe configuration.
```
livenessProbe:
  httpGet:
    path: /healthz
    port: 8080
  initialDelaySeconds: 10
  periodSeconds: 15
```
In this example, the liveness probe performs an HTTP GET request to the /healthz path on port 8080. The probe starts 10 seconds after the container starts and runs every 15 seconds thereafter.


**liveness and readinessProbe Lab** \

1. Update the newly created pod 'simple-webapp-2' with a readinessProbe using the given spec. \
Pod Name: simple-webapp-2 \
Image Name: kodekloud/webapp-delayed-start \
Readiness Probe: httpGet \
Http Probe: /ready \
Http Port: 8080 \
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: "2021-08-01T04:55:35Z"
  labels:
    name: simple-webapp
  name: simple-webapp-2
  namespace: default
spec:
  containers:
  - env:
    - name: APP_START_DELAY
      value: "80"
    image: kodekloud/webapp-delayed-start
    imagePullPolicy: Always
    name: simple-webapp
    ports:
    - containerPort: 8080
      protocol: TCP
    readinessProbe:
      httpGet:
        path: /ready
        port: 8080
```

2. Update both the pods with a livenessProbe using the given spec
Pod Name: simple-webapp-1 \
Image Name: kodekloud/webapp-delayed-start \
Liveness Probe: httpGet \
Http Probe: /live \
Http Port: 8080 \
Period Seconds: 1 \
Initial Delay: 80 \
Pod Name: simple-webapp-2 \
Image Name: kodekloud/webapp-delayed-start \
Liveness Probe: httpGet \
Http Probe: /live \
Http Port: 8080 \
Initial Delay: 80 \
Period Seconds: 1 \

`vim simple-webapp-1.yaml`
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: simple-webapp-1
  name: simple-webapp-1
spec:
  containers:
  - image: kodekloud/webapp-delayed-start
    name: simple-webapp
    ports:
    - containerPort: 8080

    livenessProbe:
      httpGet:
        path: /live
        port: 8080
      initialDelaySeconds: 80
      periodSeconds: 1
```
`kubectl apply --force -f simple-webapp-1.yaml`


`vim simple-webapp-2.yaml`

```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: simple-webapp-2
  name: simple-webapp-2
spec:
  containers:
  - image: kodekloud/webapp-delayed-start
    name: simple-webapp
    ports:
    - containerPort: 8080

    livenessProbe:
      httpGet:
        path: /live
        port: 8080
      initialDelaySeconds: 80
      periodSeconds: 1
```
`kubectl apply --force -f simple-webapp-2.yaml`

**Logging in Kubernetes** \
Logging in Kubernetes refers to the practice of capturing and storing log messages generated by applications and infrastructure components running within the Kubernetes cluster. It is an essential component of observability and helps in understanding the behavior, troubleshooting issues, and monitoring the health of applications. \
Here are some key aspects of logging in Kubernetes:
- Application Logging
- Container Runtime Logging
- Log CollectorsLog Aggregators and Storage
- Log Analysis and Visualization
- Log Retention and Compliance:

**Logging Lab**
1. A user - USER5 - has expressed concerns accessing the application. Identify the cause of the issue.Inspect the logs of the POD. \
`kubectl logs webapp-1`

2. A user is reporting issues while trying to purchase an item. Identify the user and the cause of the issue. 
Inspect the logs of the webapp in the POD. \
`kubectl logs webapp-2 -c simple-webapp`

3. How to monitor container inside of POD? \
`kubectl logs <pod_name> -c <container_name> -n <namespace>`

**Monitoring in Kubernetes** \
Monitoring in Kubernetes refers to the process of observing and collecting data about the health, performance, and behavior of your Kubernetes cluster, applications, and underlying infrastructure components. It involves gathering metrics, logs, and other relevant information to gain insights into the system's operation and identify any potential issues or bottlenecks.

Here are some key aspects of monitoring in Kubernetes: \
- Metrics Monitoring
- Health Checks
- Event Logging
- Log Monitoring
- Cluster Health Monitoring
- Alerting and Notifications
- Visualization and Dashboards:

*Monitoring Simple Lab*
*Prerequsit: have to install kubernetes Metrics Server*
1. Get node meterics
`kubectl top node`
2. Identify the node that consumes the most Memory(bytes). \
`kubectl top node --sort-by='memory' --no-headers | head -1`
3. Identify the POD that consumes the most Memory(bytes). \
`kubectl top pod --sort-by='memory' --no-headers | head -1`
4. Identify the POD that consumes the least CPU(cores). \
`kubectl top pod --sort-by='cpu' --no-headers | tail -1`

## POD Design

**Labels, Selectors & Annotations** \
Labels, selectors, and annotations are important concepts in Kubernetes that help with organizing, selecting, and providing metadata to objects within the cluster. Let's explore each of these concepts in detail:

1. Labels:
   - Labels are key-value pairs attached to Kubernetes objects like pods, services, deployments, and nodes.
   - They are used to identify and group related objects based on specific characteristics or properties.
   - Labels are flexible and can be customized to fit your application's needs.
   - Labels enable advanced features like service discovery, load balancing, and pod affinity/anti-affinity.
   - Here's an example of labels applied to a pod:

```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
  labels:
    app: backend
    tier: production
spec:
  containers:
  - name: my-app
    image: my-app-image
```

2. Selectors:
   - Selectors are used to identify and group objects based on their labels.
   - They allow you to select specific objects that match certain label criteria.
   - Selectors are used in various contexts, such as when creating services, deployments, or replica sets.
   - Selectors are expressed using label selectors, which can have equality-based requirements or set-based requirements.
   - Here's an example of a selector used in a service to target pods with specific labels

```
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app: backend
    tier: production
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8080
```

3. Annotations:
   - Annotations are additional metadata attached to Kubernetes objects.
   - Unlike labels, annotations are not used for selection or grouping but provide additional information or context.
   - Annotations can be used to store non-identifying metadata like build details, timestamps, or external system integration information.
   - Annotations can be used by operators, tools, and automation processes that require extra information about objects.
   - Here's an example of annotations added to a pod:

```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
  annotations:
    buildVersion: v1.2.3
    createdTimestamp: "2023-05-18T10:00:00Z"
spec:
  containers:
  - name: my-app
    image: my-app-image
```
Labels, selectors, and annotations provide powerful mechanisms for organizing, selecting, and augmenting Kubernetes objects. They are extensively used in various Kubernetes resources and play a crucial role in defining relationships, enabling service discovery, and providing additional context to objects within the cluster.

**Labels, Selectors & Annotations Lab** 

1. We have deployed a number of PODs. They are labelled with tier, env and bu. How many PODs exist in the dev \ environment (env)? \
`kubectl get pods --selector env=dev --no-headers | wc -l`

2. How many PODs are in the finance business unit (bu)?\
`kubectl get pod --selector bu=finance --no-headers | wc -l`

3. How many objects are in the prod environment including PODs, ReplicaSets and any other objects? \
`kubectl get all --selector env=prod --no-headers | wc -l`

4. Identify the POD which is part of the prod environment, the finance BU and of frontend tier? \
`kubectl get pod --selector env=prod,bu=finance,tier=frontend`

**Rolling Updates & Rollbacks** \
Rolling Updates and Rollbacks are important strategies in Kubernetes for managing application deployments and handling updates or rollbacks to the desired application state. Let's explore each concept in detail:

 1. Rolling Updates:
    - Rolling Updates refer to the process of updating an application in a controlled and gradual manner, ensuring high availability and minimizing downtime.
    - When performing a rolling update, Kubernetes orchestrates the update by gradually replacing the existing instances of the application with the new ones.
     - The rolling update strategy typically involves the following steps:
       - Creating a new version of the application deployment or statefulset with the desired changes.
       - Specifying the update strategy, such as the number of pods to update at a time, the maximum surge (additional pods) allowed during the update, and the maximum unavailable pods.
       - Kubernetes will gradually create new pods with the updated version while terminating the old pods in a controlled manner, ensuring a smooth transition.
       - The rolling update process continues until all the pods are updated to the new version.
     - Rolling updates allow you to ensure minimal disruption to your application during the update process and can be used to deploy new versions, scale up or down, or apply configuration changes.
 
 2. Rollbacks:
    - Rollbacks refer to the process of reverting to a previous version of an application or configuration in case of issues or failures encountered during an update.
    - In Kubernetes, rollbacks can be performed using the deployment or statefulset objects, which maintain a revision history of the desired state.
    - When a rollback is triggered, Kubernetes automatically reverts the deployment to the previous revision, effectively rolling back the application to the previous version.
    - The rollback process typically involves the following steps:
      - Identifying the desired revision to which you want to roll back.
      - Initiating the rollback action, either manually or through automation tools or CI/CD pipelines.
      - Kubernetes will take care of updating the deployment or statefulset to the previous revision, ensuring that the old pods are recreated and the application state is reverted.
      - The rollback process continues until the desired revision is restored.
    - Rollbacks provide a safety net in case issues are encountered during updates, allowing you to quickly revert to a known working state.
Both rolling updates and rollbacks are crucial strategies for managing application deployments and ensuring the availability and reliability of your applications. They enable you to perform updates seamlessly while minimizing downtime and provide a mechanism to recover from issues encountered during the update process.

Let's walk through an example of performing rolling updates and rollbacks using imperative commands in Kubernetes. 
1. Create a Deployment: \
   Create a Deployment object with the initial version (1.0) of your application. \
   `kubectl create deployment myapp-deployment --image=myapp:1.0 --replicas=3`
2. Get Deployment Details: \
    Verify the status of the Deployment and check the current version of the application. \
    `kubectl get deployment myapp-deployment`
3. Perform a Rolling Update: \
   Update the Deployment to a new version (2.0) of your application. \
   `kubectl set image deployment/myapp-deployment myapp-container=myapp:2.0`
4. Monitor Rolling Update Progress: \
   Check the rollout status to monitor the progress of the rolling update. \
   `kubectl rollout status deployment/myapp-deployment`
5. Rollback to Previous Revision: \
   In case issues are encountered during the rolling update, initiate a rollback to the previous version. \
   `kubectl rollout undo deployment/myapp-deployment`
6. Monitor Rollback Progress: \ 
   Check the rollout status to monitor the progress of the rollback. \
   `kubectl rollout status deployment/myapp-deployment`
7. Apply the updated Deployment for rolling update: \
   Apply the updated Deployment manifest using the kubectl apply command: \
   `kubectl apply -f myapp-deployment.yaml`
8. Check the rollout history: \ 
   Run the following command to view the rollout history of a Deployment: \
   `kubectl rollout history deployment/myapp-deployment`
9. Check the specific version details:
   To get detailed information about a specific revision or version, use the kubectl rollout history command with the --			revision flag: \
   `kubectl rollout history deployment/myapp-deployment --revision=<revision-number>`
10. In the example below, we are editing the deployment and changing the image from nginx:1.17 to nginx:latest while making use of the –record flag. \
    `kubectl set image deployment nginx nginx=nginx:1.17 --record`

**Kubernetes Job and Schedule** \
In Kubernetes, a **Job** is a resource that helps manage the execution of a specific task or batch job. It ensures that the task is successfully completed and provides mechanisms for controlling its execution and handling errors. Here's an overview of Jobs in Kubernetes:

1. Create a Job using this POD definition file or from the imperative command and look at how many attempts does it take to get a '6'. Use the specification given on the below.
Job Name: throw-dice-job \
Image Name: kodekloud/throw-dice \
`kubectl create job throw-dice-job --image=kodekloud/throw-dice --dry-run=client -o yaml > throw-dice-job.yaml`
```
apiVersion: batch/v1
kind: Job
metadata:
  creationTimestamp: null
  name: throw-dice-job
spec:
  template:
    metadata:
      creationTimestamp: null
    spec:
      containers:
      - image: kodekloud/throw-dice
        name: throw-dice-job
      restartPolicy: Never
  backoffLimit: 6
```
2. Update the job definition to run as many times as required to get 3 successful 6's. Delete existing job and create a new one with the given spec. Monitor and wait for the job to succeed. \
Job Name: throw-dice-job \
Image Name: kodekloud/throw-dice \
Completions: 3 \
Job Succeeded: True \
`vim throw-dice-job.yaml`
```
apiVersion: batch/v1
kind: Job
metadata:
  creationTimestamp: null
  name: throw-dice-job
spec:
  completions: 3
  template:
    metadata:
      creationTimestamp: null
    spec:
      containers:
      - image: kodekloud/throw-dice
        name: throw-dice-job
      restartPolicy: Never
  backoffLimit: 25
```
`kubectl apply --force -f throw-dice-job.yaml`

3. That took a while. Let us try to speed it up, by running upto 3 jobs in parallel. Update the job definition to run 3 jobs in parallel. \
Job Name: throw-dice-job \
Image Name: kodekloud/throw-dice \
Completions: 3 \
Parallelism: 3 \


```
apiVersion: batch/v1
kind: Job
metadata:
  creationTimestamp: null
  name: throw-dice-job
spec:
  parallelism: 3
  completions: 3
  template:
    metadata:
      creationTimestamp: null
    spec:
      containers:
      - image: kodekloud/throw-dice
        name: throw-dice-job
      restartPolicy: Never
  backoffLimit: 25
```

4. Checking Job Status \
`kubectl get jobs`

5. Viewing Job Logs. \
`kubectl logs job/myjob`

6. Deleting a Job. \
`kubectl delete job/myjob`


**Schedule:** Schedule refers to the process of specifying when and how frequently a task or job should be executed. In Kubernetes, you can use CronJobs to schedule recurring tasks. CronJobs are a type of Kubernetes resource that allow you to define tasks based on the cron format. CronJobs create Jobs at specified intervals based on the specified schedule. They are commonly used for automated tasks, periodic jobs, or scheduled backups.

1. Let us now schedule that job to run at 21:30 hours every day. Create a CronJob for this. \
CronJob Name: throw-dice-cron-job \
Image Name: kodekloud/throw-dice \
Schedule: 30 21 * * * \
`vim throw-dice-job.yaml`

```
apiVersion: batch/v1
kind: CronJob
metadata:
  name: throw-dice-cron-job
spec:
  schedule: "30 21 * * *"
  jobTemplate:
    spec:
      parallelism: 3
      completions: 3
      backoffLimit: 25
      template:
          spec:
            containers:
            - image: kodekloud/throw-dice
              name: throw-dice-job
            restartPolicy: Never
```
`kubectl create -f throw-dice-job.yaml`

2. Imperative command for creating job and cronjob in Kubernetes \
`kubectl create cronjob my-job  --schedule="0,15,30,45 * * * *" --image=busy-box`


## Kubernetes Service
In Kubernetes, a Service is an abstracted way to expose and provide network connectivity to a set of pods. It allows pods to have a stable network identity and provides load balancing and service discovery functionalities. Here's an overview of Kubernetes Services. \
1. Service Types:
   - Kubernetes supports different types of Services, including ClusterIP, NodePort, LoadBalancer, and ExternalName.
   - ClusterIP: Exposes the Service on a cluster-internal IP, only accessible within the cluster.
   - NodePort: Exposes the Service on a static port on each node's IP, allowing external access to the Service.
   - LoadBalancer: Creates an external load balancer (e.g., cloud provider's load balancer) to expose the Service externally.
   - ExternalName: Maps the Service to an external DNS name, enabling access to external services. 
 
**Here's an example of creating a Kubernetes Service with NodePort for a Deployment**
 ```
 apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  type: NodePort
  selector:
    app: my-app
  ports:
    - name: http
      port: 80
      targetPort: 8080
      nodePort: 30000
 ```
In this example, the Kubernetes Service is named "my-service" and has the following configuration.

- `spec.type` is set to `NodePort`, indicating that the Service will be of type NodePort, which exposes the Service on a static port on each node.
- `spec.selector` specifies the label selector used to match the pods that the Service will route traffic to. In this case, pods with the label `app: my-app` will be selected.
- `spec.ports` defines the port mapping for the Service. It contains one port definition:
- `name: http` sets a name for the port, which can be used for identification purposes.
- `port: 80` specifies the port number on which the Service will be exposed externally. Any incoming traffic targeting this port will be forwarded to the pods.
- `targetPort: 8080` indicates the port number on the pods to which the incoming traffic will be forwarded. The pods should have a container listening on port 8080 to handle the traffic.
- `spec.ports` also includes `nodePort: 30000` which specifies the static port number on each node. In this case, the Service will be accessible externally on port 30000 on each node. \
With this configuration, the Service will route incoming traffic on port 80 (external) to port 8080 on the pods that match the label selector. The Service will be exposed on port 30000 on each node, allowing external access to the Service.

**Here's an example of a Kubernetes Service using the ClusterIP type, along with an explanation**
```
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  type: ClusterIP
  selector:
    app: my-app
  ports:
    - name: http
      port: 80
      targetPort: 8080
```
- The Service is named "my-service".
- `spec.type` is set to `ClusterIP`, indicating that the Service will only be accessible from within the cluster.
- The `spec.selector` field specifies the label selector used to match the pods that the Service will route traffic to. In this case, it selects pods with the label `app: my-app`.
- The `spec.ports` section defines the port mapping. Here, port 80 is exposed by the Service, and it will be forwarded to port 8080 on the pods. \
When you create this Service, Kubernetes assigns it a cluster IP address. The Service will act as an internal endpoint, allowing other components within the cluster to communicate with the pods through the Service.

**Servie Lab Test**
1. How many Services exist on the system? \
`kubectl get svc`
2. What is the type of the default kubernetes service? \
`kubectl get service`
3. What is the targetPort configured on the kubernetes service? \
`kubectl describe service kubernetes`
4. How many labels are configured on the kubernetes service? \
`kubectl describe service kubernetes `
5. How many Endpoints are attached on the kubernetes service? \
`kubectl describe service kubernetes `
6. How many Deployments exist on the system now? \
`kubectl get deploy`
7. What is the image used to create the pods in the deployment? \
`kubectl describe deployments.apps simple-webapp-deployment`
8. Create a new service to access the web application using the service-definition-1.yaml file. \ 
Name: webapp-service \
Type: NodePort \
targetPort: 8080 \
port: 8080 \
nodePort: 30080 \
selector: \
  name: simple-webapp

```
---
apiVersion: v1
kind: Service
metadata:
  name: webapp-service
  namespace: default
spec:
  ports:
  - nodePort: 30080
    port: 8080
    targetPort: 8080
  selector:
    name: simple-webapp
  type: NodePort
```

**Network Policies in Kubernetes** \
Network Policies in Kubernetes allow you to define rules that control network traffic to and from pods. They help enforce security and isolation within your cluster. \
Network Policies are enforced by a network plugin in your Kubernetes cluster. The availability and support for Network Policies depend on the specific network plugin you are using, such as Calico, Cilium, or kube-router. \
Here's an example of a Network Policy along with an explanation.
```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: my-network-policy
spec:
  podSelector:
    matchLabels:
      app: my-app
  ingress:
    - from:
        - podSelector:
            matchLabels:
              role: backend
        - namespaceSelector:
            matchLabels:
              name: production
  egress:
    - to:
        - podSelector:
            matchLabels:
              app: database
```
[Network Policy Reference](https://kubernetes.io/docs/concepts/services-networking/network-policies) \
*In this example:*
- The Network Policy is named "my-network-policy" and applies to pods that have the label `app: my-app`. 
- `spec.podSelector` specifies the pods to which the Network Policy is applied. In this case, pods with the label `app: my-app` will be affected by this policy.
- `spec.ingress` defines the ingress rules that control incoming traffic to the pods. Here, two rules are defined:
- The first rule allows incoming traffic from pods that have the label `role: backend`.
- The second rule allows incoming traffic from pods in the "production" namespace.
- `spec.egress` specifies the egress rules that control outgoing traffic from the pods. In this example, there is one rule:
- The rule allows outgoing traffic to pods that have the label `app: database`. 
With this Network Policy, the following traffic is allowed. \
- Incoming traffic to pods with the label `app: my-app` is allowed from pods with the label role: backend and from any pods in the "production" namespace.
- Outgoing traffic from pods with the label `app: my-app` is allowed to pods with the label app: database.

**Network Policy Lab**
1. How many network policies do you see in the environment? \
`kubectl get networkpolicies.networking.k8s.io`
2. What is the name of the Network Policy? \
```
kubectl get networkpolicies.networking.k8s.io \
kubectl get netpol
```
3. Which pod is the Network Policy applied on? \
`kubectl describe netpol payroll-policy`
4. What type of traffic is this Network Policy configured to handle? \
`kubectl describe networkpolicy`
5. What is the impact of the rule configured on this Network Policy? \
`kubectl describe netpol payroll-policy`
6. Create a network policy to allow traffic from the Internal application only to the payroll-service and db-service.
Use the spec given below. You might want to enable ingress traffic to the pod to test your rules in the UI. \
Policy Name: internal-policy \
Policy Type: Egress \
Egress Allow: payroll \
Payroll Port: 8080 \
Egress Allow: mysql \
MySQL Port: 3306

![Network Policy Scenario](/image/network-policy.jpg)

```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: internal-policy
  namespace: default
spec:
  podSelector:
    matchLabels:
      name: internal
  policyTypes:
    - Egress
  egress:
    - to:
        - podSelector:
            matchLabels:
              name: payroll
      ports:
        - protocol: TCP
          port: 8080
    - to:
        - podSelector:
            matchLabels:
              name: mysql
      ports:
        - protocol: TCP
          port: 3306
```
**Ingress Lab**
1. To list all the Ingress resources in a Kubernetes cluster. \
`kubectl get ingress --all-namespaces`
2. Which namespace is the Ingress Controller deployed in? \
*Identify the namespace of Ingress Controller* \
`kubectl get all -A`
3. What is the name of the Ingress Controller Deployment? \
`kubectl get deploy --all-namespaces`
4. How many applications are deployed in the app-space namespace? \
`kubectl get deploy --all-namespaces`
5. Which namespace is the Ingress Resource deployed in? \
`kubectl get ingress --all-namespaces` 
6. What is the name of the Ingress Resource?
`kubectl get ingress --all-namespaces` 
7. What is the Host configured on the Ingress Resource? \
`kubectl describe ingress ingress-wear-watch -n app-space`
8. Identify and implement the best approach to making this application available on the ingress controller and test to make sure its working. Look into annotations: rewrite-target as well. \
Ingress Created \
Path: /pay \
Configure correct backend service \
Configure correct backend port 
```
kubectl get deploy --all-namespaces 
kubectl get svc -n critical-space
```
```
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: test-ingress
  namespace: critical-space
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
spec:
  rules:
  - http:
      paths:
      - path: /pay
        pathType: Prefix
        backend:
          service:
           name: pay-service
           port:
            number: 8282
```

## State Persistence for Kubernetes

**Docker layer Architecture** 
Persistence in Kubernetes refers to the ability to store and maintain the stateful data of applications running in Kubernetes clusters. Stateful applications require persistent storage to preserve their data across pod restarts, scaling events, or node failures. Kubernetes provides several mechanisms for achieving data persistence.
- Volumes
- PersistentVolumeClaims (PVCs)
- StatefulSets

Let's walk through an example of the Docker layer architecture using a simple `Dockerfile`
```
FROM ubuntu:latest
RUN apt-get update && apt-get install -y curl
COPY script.sh /app/
WORKDIR /app
CMD ["./script.sh"]
```
In this example:
1. Base Image Layer: The FROM ubuntu:latest instruction specifies the base image layer, which is an Ubuntu image obtained from the Docker registry. This layer contains the essential files and dependencies of the Ubuntu operating system.
2. Intermediate Layers: The RUN instruction installs the curl package by executing the apt-get commands. This instruction adds a new layer on top of the base image layer, representing the installation of the package. \
The COPY instruction copies the script.sh file from the host machine to the /app/ directory in the image. This instruction adds another layer to the image, representing the file copy operation. \
The WORKDIR instruction sets the working directory to /app/. This instruction does not create a new layer since it only modifies the container's metadata. \
3. Writable Layer (Container Layer): When a container is created from the image built with the Dockerfile, a writable layer is added on top of the image's layers. Any changes made within the container, such as executing the script, will be stored in this writable layer. \
During the image build process, Docker builds each instruction in the Dockerfile and creates intermediate layers. These layers are stacked on top of each other, forming the image. Each layer is read-only and represents a specific modification or change.

**Docker Storage**
In Docker, storage refers to the management of data within containers and the persistence of that data beyond the lifetime of a container. Docker provides several mechanisms for managing storage:
1. Volumes: Volumes are the preferred way to persist data in Docker containers. A volume is a directory that is managed by Docker and can be mounted into one or more containers. Volumes are independent of container lifecycles and can be shared among containers. They provide a reliable and efficient way to store and share data between containers or between the host machine and containers. Volumes can be managed using Docker CLI commands or specified in Docker Compose or Kubernetes configurations.
2. Bind Mounts: Bind mounts are a way to directly mount a file or directory from the host machine into a container. With bind mounts, the files and directories on the host are mapped to a path inside the container. Any changes made to the files or directories are immediately visible to both the host and the container. Bind mounts are useful when you want to access host files or share data between the host and the container.
3. Tmpfs Mounts: Tmpfs mounts allow you to mount a temporary file system in a container's memory. This means that the data stored in a tmpfs mount is not persisted beyond the lifetime of the container. Tmpfs mounts are suitable for storing temporary or cache data that doesn't need to be preserved. \
When using volumes and bind mounts, it's important to consider the following: \
Volumes provide better performance and flexibility compared to bind mounts since they are managed by Docker and can be easily shared among containers. \
Bind mounts provide direct access to the host file system, which can be useful for development or debugging purposes but may introduce dependencies on host-specific paths. \
Both volumes and bind mounts can be used together in the same container to achieve a combination of persistent and host-specific storage.

**Docker Storage Driver** \
Docker, the storage driver is a component responsible for managing how the container images and writable layers are stored and accessed on the host machine. The storage driver determines how Docker interacts with the underlying storage infrastructure. \
Docker supports multiple storage drivers, each with its own characteristics and capabilities. The choice of storage driver can impact various aspects of container performance, storage efficiency, and compatibility with different operating systems and storage technologies. 
Here are some commonly used Docker storage drivers:
- Overlay2: Overlay2 is the default storage driver for Docker on most Linux distributions. It uses the overlayFS file system to create layered images and provides copy-on-write functionality for efficient use of disk space. Overlay2 is known for its performance, scalability, and compatibility with most Linux distributions.
- AUFS: AUFS (Advanced Multi-Layered Unification Filesystem) was one of the earliest storage drivers used by Docker. It supports layered file systems and copy-on-write functionality. However, AUFS has limitations, such as being incompatible with some newer Linux kernel versions and lacking official support in some distributions.
- Device Mapper: The Device Mapper storage driver uses the device mapper kernel module to manage thin provisioning and snapshot capabilities. It provides performance and stability but requires additional configuration and management of logical volumes.
- Btrfs: Btrfs (B-tree file system) is a copy-on-write file system that can be used as a storage driver in Docker. Btrfs supports snapshotting, compression, and other advanced features. However, it may require specific kernel support and is less commonly used compared to other drivers.
- ZFS: ZFS (Zettabyte File System) is a high-performance file system that offers advanced storage features such as snapshots, data integrity checks, and efficient storage allocation. Docker can use ZFS as a storage driver, but it requires specific kernel modules and configuration. \ 
The choice of the storage driver depends on factors such as the operating system, compatibility, performance requirements, and available storage technologies. It's recommended to use the default Overlay2 driver for most use cases, as it offers good performance and compatibility. \
You can specify the storage driver when configuring Docker by modifying the Docker daemon configuration file, typically located at /etc/docker/daemon.json. You can set the storage-driver option to the desired driver name. \
It's important to note that the availability of storage drivers may vary depending on the host operating system and Docker version. It's recommended to refer to the official Docker documentation and the specific documentation for your operating system to understand the available options and recommended configuration. \

**Volume Driver Plugins in Docker** \
Volume driver plugins in Docker allow you to extend Docker's functionality by integrating with external storage systems and providing custom volume types. Volume driver plugins enable you to use different storage technologies, such as cloud storage platforms, network-attached storage (NAS), or distributed file systems, as Docker volumes. \
With volume driver plugins, you can create and manage volumes backed by external storage systems, providing features like dynamic provisioning, data encryption, snapshotting, and replication. These plugins integrate with Docker's volume management framework, allowing you to use the familiar Docker commands and APIs to interact with the volumes.

**Volumes in kubernetes** \
In Kubernetes, volumes provide a way to persist data beyond the lifecycle of a single container. They allow you to decouple storage from individual pods and enable data sharing and persistence between containers within a pod. Here's an overview of volumes in Kubernetes. \
1. Volume Types: Kubernetes supports various types of volumes, each with its own characteristics and use cases. Some common volume types include:
- EmptyDir: A temporary volume that is created when a pod is scheduled on a node and exists for the lifetime of that pod.
- HostPath: Mounts a directory or file from the host node's filesystem into the pod.
- PersistentVolume: A cluster-level resource that represents a piece of networked storage provisioned by an administrator.
- ConfigMap: Allows you to inject configuration data into a pod as files.
- Secret: Similar to ConfigMap, it allows you to inject sensitive information, such as passwords or API keys, into a pod as files.

2. Declaring Volumes: Volumes are declared in the volumes section of a pod or deployment configuration. You specify the volume type and any required configuration parameters. 

3. Mounting Volumes: To access the data stored in a volume, you need to mount the volume to a specific path within the container. This is done using the volumeMounts field in the container specification. Multiple containers within a pod can share the same volume. 

4. Dynamic Provisioning: Kubernetes also supports dynamic provisioning of persistent volumes (PVs) using storage classes. This allows automatic provisioning of storage resources based on defined policies and specifications. Persistent volumes provide durable and networked storage that persists beyond the lifetime of a pod or container.

5. Lifecycle and Management: Volumes in Kubernetes can be managed independently of the pod or container lifecycle. They can be attached, detached, and modified without affecting the running containers. Volumes can also be resized, snapshots can be taken, and data can be migrated between different types of volumes.

Volumes in Kubernetes provide a flexible and scalable way to handle data storage and persistence requirements for containers and pods. They enable data sharing, decoupling of storage from individual containers, and support for various storage types and configurations.

**Kubernetes Volumes Eample**
```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
    - name: my-container
      image: my-image
      volumeMounts:
        - name: data-volume
          mountPath: /data
  volumes:
    - name: data-volume
      emptyDir: {}
```
Let's break down the components of this configuration: \
`apiVersion: v1`: Specifies the API version of the Kubernetes object. In this case, it is `v1`, which corresponds to the core/v1 API group. \
`kind: Pod`: Specifies that this object is of type Pod. \
`metadata:` Contains metadata information about the Pod, such as the name (my-pod). \
`spec:` Describes the desired state of the Pod. \
`containers:` Specifies the list of containers to run inside the Pod. In this case, there is a single container named my-container. \
`name:` Specifies the name of the container. \
`image:` Specifies the container image to run (my-image). \
`volumeMounts:` Defines the volume mounts for the container. \
`name:` Specifies the name of the volume to mount (data-volume). \
`mountPath:` Specifies the path within the container where the volume will be mounted (/data). \
`volumes:` Specifies the list of volumes to be used by the Pod. \
`name:` Specifies the name of the volume (data-volume). \
`emptyDir:` Specifies an EmptyDir volume type. This means that an empty directory will be created and mounted as the volume for the Pod. \

**PersistentVolume (PV) in Kubernetes** \
A PersistentVolume (PV) in Kubernetes is a storage abstraction that represents a piece of storage provisioned in the cluster. It provides a way to decouple storage configuration from Pod definitions, allowing storage to be managed separately and shared among multiple Pods.
Here are some key points to understand about PersistentVolumes:
- Provisioned Storage: A PersistentVolume represents a volume that has been provisioned in the cluster by an administrator or through a storage provisioner. It could be physical storage, network-attached storage (NAS), cloud-based storage, or any other storage solution compatible with Kubernetes. 
- Lifecycle: PersistentVolumes have an independent lifecycle from the Pods that use them. They persist even if the Pods are deleted or rescheduled. This allows data to be retained across Pod restarts and ensures data durability. 
- Storage Classes: PersistentVolumes are associated with a storage class, which defines the underlying storage provider and configuration options. Storage classes provide a way to dynamically provision and manage storage based on different requirements, such as performance, availability, or cost. 
- Access Modes: PersistentVolumes support different access modes to define how the volume can be mounted and used by Pods. The common access modes include ReadWriteOnce (can be mounted by a single node as read-write), ReadOnlyMany (can be mounted by multiple nodes as read-only), and ReadWriteMany (can be mounted by multiple nodes as read-write). 
- Capacity and Reclaim Policy: PersistentVolumes have a specified capacity that determines the amount of storage available. They also have a reclaim policy that determines what happens to the data when the PersistentVolume is released. The reclaim policy can be set to Retain (data is retained), Delete (data is deleted), or Recycle (data is wiped and volume can be reused). 
- Binding to PersistentVolumeClaims: PersistentVolumes are bound to PersistentVolumeClaims (PVCs), which represent a request for storage by a Pod. When a PVC is created, Kubernetes matches it to an available PersistentVolume that meets the requested capacity, access mode, and storage class. Once bound, the PVC can be used by the Pod to access the PersistentVolume. 
PersistentVolumes provide a flexible and scalable way to manage storage in Kubernetes. They enable administrators to manage and allocate storage resources independently of the application Pods, allowing for better resource utilization and separation of concerns.

**Here's an example of creating a PersistentVolume (PV) using NFS as the storage backend:**
```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: my-pv
spec:
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Retain
  nfs:
    server: 192.168.0.100
    path: /exports/data
```
[Referance](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#reserving-a-persistentvolume)
*In this example:* \
`metadata.name:` Specifies the name of the PersistentVolume (my-pv). \
`spec.capacity.storage:` Specifies the capacity of the PersistentVolume, which is 10Gi in this case. \
`spec.accessModes:` Specifies the access modes for the PersistentVolume. ReadWriteMany allows multiple Pods to read from and write to the volume simultaneously. \
`spec.persistentVolumeReclaimPolicy:` Specifies the reclaim policy for the PersistentVolume. Retain means that the volume will not be automatically deleted when released. \
`spec.nfs.server:` Specifies the IP address or hostname of the NFS server. \
`spec.nfs.path:` Specifies the path on the NFS server where the data is stored.

Once you have defined the PersistentVolume, you can create a PersistentVolumeClaim (PVC) to dynamically bind to the PV: \
**Create a PersistentVolumeClaim (PVC)**
```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-pvc
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 5Gi
```
[Referance](https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/#create-a-persistentvolume) \
`apiVersion: v1:` Specifies the API version of the resource, in this case, a PersistentVolumeClaim using the core/v1 API version. \
kind: PersistentVolumeClaim: Indicates that this YAML defines a PersistentVolumeClaim. \
`metadata: name: my-pvc:` Sets the name of the PersistentVolumeClaim to "my-pvc". \
`spec: accessModes: - ReadWriteMany:` Specifies the access modes for the PersistentVolumeClaim. In this example, it is set to "ReadWriteMany", which allows multiple Pods to read from and write to the same PVC simultaneously. This is commonly used for shared storage. \
`resources: requests: storage: 5Gi:` Defines the storage resources requested by the PersistentVolumeClaim. In this case, the claim is requesting a storage capacity of "5Gi" (5 gigabytes). \
Once you apply this YAML using the `kubectl apply` command, Kubernetes will attempt to create a PersistentVolumeClaim with the specified configuration. The PVC can then be used by Pods to request storage from a suitable PersistentVolume.

Next, let's configure a **Deployment** to use the PersistentVolumeClaim for persistent storage: 
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
        - name: my-container
          image: my-image
          ports:
            - containerPort: 8080
          volumeMounts:
            - name: data-volume
              mountPath: /data
      volumes:
        - name: data-volume
          persistentVolumeClaim:
            claimName: my-pvc
```
In this example: \
We have a Deployment named `my-deployment` that manages a group of Pods. \
The Deployment's template specifies a container named `my-container` running the `my-image` image and listening on port 8080. \
The `volumeMounts` section defines a `volume mount named `data-volume` that mounts the PVC's storage at the path `/data within the container. \
The volumes section specifies a `volume` named `data-volume` that references the PersistentVolumeClaim `(my-pvc)` created earlier.

When you apply this Deployment YAML using the kubectl apply command, Kubernetes will create the PersistentVolumeClaim `(my-pvc)` if it doesn't exist. Then, the Deployment will create the specified number of Pods and mount the PVC's storage to the /data path within each Pod.

**Kubernetes Volume Lab**
1. Check application stores logs at location /log/app.log. View the logs. \
`kubectl exec webapp -- cat /log/app.log`
2. Configure a volume to store these logs at /var/log/webapp on the host.
Use the spec provided below. \
Name: webapp \
Image Name: kodekloud/event-simulator \
Volume HostPath: /var/log/webapp \
Volume Mount: /log \
```
kubectl run webapp --image=kodekloud/event-simulator --dry-run=client -o yaml > 4.yaml
vim 4.yaml
```
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: webapp
  name: webapp
spec:
  containers:
  - image: kodekloud/event-simulator
    name: webapp
    volumeMounts:
    - mountPath: /log
      name: test-volume
  volumes:
  - name: test-volume
    hostPath:
      # directory location on host
      path: /var/log/webapp
      type: Directory
```
[Reference](https://kubernetes.io/docs/concepts/storage/volumes/#hostpath)

3. Create a Persistent Volume with the given specification. \
Volume Name: pv-log \
Storage: 100Mi \
Access Modes: ReadWriteMany \
Host Path: /pv/log \
Reclaim Policy: Retain 
```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-log
spec:
  capacity:
    storage: 100Mi
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Retain
  hostPath:
    path: "/pv/log"
```
[Reference_1](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumes-typed-hostpath) 
[Reference_2](https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/#create-a-persistentvolume)

4. Let us claim some of that storage for our application. Create a Persistent Volume Claim with the given specification. \
Volume Name: claim-log-1 \
Storage Request: 50Mi \
Access Modes: ReadWriteOnce 
```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: claim-log-1
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 50Mi
```
5. What is the state of the Persistent Volume Claim? \
`kubectl get pvc`
6. What is the state of the Persistent Volume? \
`kubectl get pv`
7. Update the webapp pod to use the persistent volume claim as its storage. \
Replace hostPath configured earlier with the newly created PersistentVolumeClaim. \
Name: webapp \
Image Name: kodekloud/event-simulator \
Volume: PersistentVolumeClaim=claim-log-1 \
Volume Mount: /log \
`kubectl delete po webapp`
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: webapp
  name: webapp
spec:
  volumes:
    - name: claim-log-1
      persistentVolumeClaim:
        claimName: claim-log-1
  containers:
  - image: kodekloud/event-simulator
    name: webapp
    volumeMounts:
        - mountPath: "/log"
          name: claim-log-1
```
8. What is the Reclaim Policy set on the Persistent Volume pv-log? \
`kubectl get pv`
 






